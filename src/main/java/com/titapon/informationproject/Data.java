/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.titapon.informationproject;

/**
 *
 * @author home
 */
public class Data {

    @Override
    public String toString() {
        return "Data{" + "Name=" + Name + ", Surname=" + Surname + ", Nickname=" + Nickname + ", age=" + age + ", Birthday=" + Birthday + ", Address=" + Address + ", Education=" + Education + '}';
    }

    public Data(String Name, String Surname, String Nickname, int age, int Birthday, String Address, String Education) {
        this.Name = Name;
        this.Surname = Surname;
        this.Nickname = Nickname;
        this.age = age;
        this.Birthday = Birthday;
        this.Address = Address;
        this.Education = Education;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public String getNickname() {
        return Nickname;
    }

    public void setNickname(String Nickname) {
        this.Nickname = Nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getBirthday() {
        return Birthday;
    }

    public void setBirthday(int Birthday) {
        this.Birthday = Birthday;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String Education) {
        this.Education = Education;
    }
    private String Name;
    private String Surname;
    private String Nickname;
    private int age;
    private int Birthday;
    private String Address;
    private String Education;
    
}
